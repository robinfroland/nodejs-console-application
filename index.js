const fileReader = require('./file-reader');
const osInfo = require('./os-info');
const server = require('./server');
const readline = require('readline');


function openMenu() {
    const reader = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    })
    
    const menu = "Choose an option:\n" +
                "1. Read package.json\n" +
                "2. Display OS info\n" +
                "3. Start HTTP server\n" +
                "Type a number: ";
    
    reader.question(menu, (answer) => {
        switch (parseInt(answer)) {
            case 1:
                fileReader.log('package.json')
                break;
            case 2:
                osInfo.log();
                break;
            case 3:
                server.startServer();
                break;
            default:
                console.log("Invalid option");
                break;
        }
        reader.close();
    });       
}

openMenu();
