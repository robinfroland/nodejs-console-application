const fs = require('fs');

module.exports.log = (file) => {
    console.log("Reading " + file + "...\n");
    fs.readFile(file, (error, data) => {
        try {
            console.log(JSON.parse(data));
        } catch (error) {
            console.log(error);
        }
    });
}
    



