const http = require('http');

module.exports.startServer = () => {
    console.log("Starting HTTP server...\n");
    const server = http.createServer((request, response) => {
        response.write("Hello World")
        response.end();
    })
    server.listen(PORT = 3000, () => console.log("Listening on port " + PORT));
}