const os = require('os');

module.exports.log = () => {
    console.log("Getting OS info...\n");
    const sysMem = (os.totalmem() / 1024 / 1024 / 1024).toFixed(2);
    const freeMem = (os.freemem() / 1024 / 1024 / 1024).toFixed(2);
    const nCores = os.cpus().length;
    const arch = os.arch();
    const platform = os.platform();
    const release = os.release();
    const user = os.userInfo().username;

    console.log('--- SYSTEM INFORMATION --- \n\n' +
        'SYSTEM MEMORY: ' + sysMem + ' GB \n' +
        'FREE MEMORY: ' + freeMem + ' GB \n' +
        'CPU CORES: ' + nCores + '\n' +
        'ARCH: ' + arch + '\n' +
        'PLATFORM: ' + platform + '\n' +
        'RELEASE: ' + release + '\n' +
        'USER: ' + user);
};